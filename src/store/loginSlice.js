import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import AuthService from "../service/AuthService";
import axios from "axios";
import { API_URL } from "../http";

export const login = createAsyncThunk(
  "auth/login",
  async function (user, { rejectWithValue, dispatch }) {
    const { email, password } = user;
    try {
      const response = await AuthService.login(email, password);
      console.log("login", response);
      localStorage.setItem("token", response.data.accessToken);
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);

export const registration = createAsyncThunk(
  "auth/registration",
  async (user, { rejectWithValue, dispatch }) => {
    const { email, password } = user;
    try {
      const response = await AuthService.registration(email, password);
      console.log("res", response);
      localStorage.setItem("token", response.data.accessToken);
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      rejectWithValue(e.message);
    }
  }
);

export const logout = createAsyncThunk(
  "auth/logout",
  async function (_, { rejectWithValue, dispatch }) {
    try {
      const response = await AuthService.logout();
      localStorage.removeItem("token");
      dispatch(setUser({}));
      dispatch(setAuth(false));
      return response.data;
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);

export const checkAuth = createAsyncThunk(
  "auth/checkAuth",
  async function (_, { rejectWithValue, dispatch }) {
    try {
      const response = await axios.get(`${API_URL}/refresh`, {
        withCredentials: true,
      });
      console.log("auth", response);
      localStorage.setItem("token", response.data.accessToken);
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);

const loginSlice = createSlice({
  name: "auth",
  initialState: {
    user: null,
    isAuth: false,
    isErrors: null,
  },
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
    setAuth(state, action) {
      state.isAuth = action.payload;
    },
  },
  extraReducers: {
    [login.pending]: (state, action) => {},
    [login.fulfilled]: (state, action) => {},
    [registration.rejected]: (state, action) => {
      state.isErrors = action.payload;
    },
  },
});

export default loginSlice.reducer;
export const { setUser, setAuth } = loginSlice.actions;
