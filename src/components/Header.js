import React, { useEffect } from "react";
import { Button } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../store/loginSlice";
import { Menus } from "./Menu";
export const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.login.isAuth);
  const user = useSelector((state) => state.login.user);

  useEffect(() => {
    if (isAuth) {
      navigate("./");
    }
  }, [isAuth]);

  function toSignIn() {
    navigate("./sign-in");
  }
  function toSignUp() {
    navigate("./sign-up");
  }

  const logoutIn = () => {
    dispatch(logout());
  };
  return (
    <div className="header__wrapper">
      <div className="header__menu">
        <Link className="header__logo" to="./">
          Estimates
        </Link>
        <Menus className="link__menu" />
      </div>

      {isAuth ? (
        <div className="header__button">
          <p className="header__user">{user.email}</p>
          <Button onClick={logoutIn}>Выход</Button>
        </div>
      ) : (
        <div className="header__button">
          <Button onClick={toSignIn}>Вход</Button>
          <Button onClick={toSignUp}>Регистрация</Button>
        </div>
      )}
    </div>
  );
};
