import { DownOutlined, SmileOutlined } from "@ant-design/icons";
import { Dropdown, Menu, Space } from "antd";
import React from "react";
import { Link } from "react-router-dom";
const menu = (
  <Menu
    items={[
      {
        key: "1",
        label: <Link to="./">Добавить время</Link>,
      },
      {
        key: "2",
        label: <Link to="./list-estimate">Свисок внесенных часов</Link>,
      },
      {
        key: "3",
        label: (
          <Link to="./list-estimate">Получить список всех пользователей</Link>
        ),
      },
    ]}
  />
);

export const Menus = () => (
  <Dropdown overlay={menu}>
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        Проекты
        <DownOutlined />
      </Space>
    </a>
  </Dropdown>
);
