import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { checkAuth, login, logout, registration } from "../store/loginSlice";

export const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.login.isAuth);
  const user = useSelector((state) => state.login.user);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      dispatch(checkAuth());
    }
  }, []);

  function loginIn() {
    dispatch(login({ email, password }));
  }
  function registrationIn() {
    dispatch(registration({ email, password }));
  }
  function logoutIn() {
    dispatch(logout());
  }

  return (
    <div>
      <h1>
        {isAuth ? `Пользователь авторизован ${user.email}` : "Авторизуйтесь"}
      </h1>
      <input
        onChange={(e) => {
          setEmail(e.target.value);
        }}
        value={email}
        type="text"
        placeholder="Email"
      ></input>
      <input
        onChange={(e) => setPassword(e.target.value)}
        value={password}
        type="text"
        placeholder="Password"
      ></input>
      <button onClick={loginIn}>Login</button>
      <button onClick={registrationIn}>Registration</button>
      <button onClick={logoutIn}>Выход</button>
    </div>
  );
};
