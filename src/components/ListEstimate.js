import React, { useState } from "react";
import { useDispatch } from "react-redux";
import UserService from "../service/UserService";

const ListEstimate = () => {
  const [users, setUsers] = useState([]);
  const dispatch = useDispatch();

  async function getAllUsers() {
    try {
      console.log("1", users);
      const response = await UserService.fetchUsers();
      console.log("1.5", response);
      setUsers(response.data);
      console.log("2", users);
    } catch (e) {
      console.log(e);
    }
  }
  return (
    <div>
      <button onClick={getAllUsers}>Получить список</button>
      {users.map((user) => (
        <div key={user.email}>{user.email}</div>
      ))}
    </div>
  );
};

export default ListEstimate;
