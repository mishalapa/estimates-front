import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { checkAuth, login } from "../store/loginSlice";
import { Form, Input, Button } from "antd";

export const SignIn = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    if (localStorage.getItem("token")) {
      dispatch(checkAuth());
    }
  }, []);

  const onSubmit = (identification) => {
    const { email, password } = identification;
    dispatch(login({ email, password }));
    console.log(identification);
    console.log("indet", email, password);
  };

  return (
    <div className="sign__wrapper">
      <h1>Войти</h1>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
