import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Form, Input, Button, DatePicker } from "antd";
import TextArea from "antd/lib/input/TextArea";

export const Main = () => {
  const isAuth = useSelector((state) => state.login.isAuth);
  const navigate = useNavigate();
  if (!isAuth) {
    navigate("./sign-in");
  }
  const onSubmit = (identification) => {
    console.log(identification["date-time-picker"]["_d"]);
  };

  const config = {
    rules: [
      {
        type: "object",
        required: true,
        message: "Please select time!",
      },
    ],
  };

  return (
    <div className="estimate__wrapper">
      <h1>Добавить время</h1>
      <Form
        className="estimate__form"
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onSubmit}
        autoComplete="off"
        layout={"vertical"}
        initialValues={{
          layout: "vertical",
        }}
      >
        <Form.Item name="date-time-picker" label="Дата и Время" {...config}>
          <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
        </Form.Item>

        <Form.Item
          className="estimate__input"
          label="Затраченное время"
          name="time"
          rules={[
            { required: true, message: "Введите время, затраченное на работу" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          className="estimate__input"
          label="Комментарий"
          name="comment"
          rules={[
            { required: true, message: "Опишите работу, выполненную за день" },
          ]}
        >
          <TextArea rows={4} />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Записать
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
